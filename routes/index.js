var express = require("express");
var router = express.Router();
const { transporter } = require("../configs/email");
/* GET home page. */
require("dotenv").config();
router
  .get("/", function (req, res, next) {
    res.render("index", { title: "Express" });
  })
  .post("/api/send/email", function (req, res, next) {
    const { email, subject, text } = req.body;
    var mailOptions = {
      from: process.env.EMAIL_USER,
      to: email,
      subject: subject,
      text: text,
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        res.status(500).send(error.message);
      } else {
        res.status(200).send("Email sent: " + email + " " + info.response);
      }
    });
  });

module.exports = router;
